# What is this?

A small Ada library for random generation that exploits (if possible) any strong generator present in the system, if it is not possible it falls back to the Ada library random generator
