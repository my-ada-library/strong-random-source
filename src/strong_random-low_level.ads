private package Strong_Random.Low_Level is
   type Byte is mod 256;

   function Random_Byte return Byte;
end Strong_Random.Low_Level;
