pragma Ada_2012;
with Strong_Random.Low_Level;

package body Strong_Random is
   use type Low_Level.Byte;
   use Interfaces;

   --------------------
   -- Generic_Random --
   --------------------

   function Generic_Random return Data_Type
   is
      use Low_Level;

      type Accumulator_Type is mod System.Max_Binary_Modulus;

      Partial : constant Natural := Data_Type'Size mod 8;
      N_Bytes : constant Natural := (Data_Type'Size - Partial) / 8;
      Is_Binary : constant Boolean := 2 ** Data_Type'Size = Data_Type'Modulus;

      Result : Accumulator_Type;
   begin
      loop
         Result :=  (if Partial > 0 then
                        Accumulator_Type (Random_Byte) and (2 ** Partial - 1)
                     else
                        0);

         for K in 1 .. N_Bytes loop
            Result := 256 * Result + Accumulator_Type (Random_Byte);
         end loop;

         exit when Is_Binary or else Result < Data_Type'Modulus;
      end loop;

      return Data_Type (Result);
   end Generic_Random;

   ------------
   -- Random --
   ------------

   function Random return Unsigned_8
   is
      function Rnd is new Generic_Random (Unsigned_8);
   begin
      return Rnd;
   end Random;

   ------------
   -- Random --
   ------------

   function Random return Unsigned_16
   is
      function Rnd is new Generic_Random (Unsigned_16);
   begin
      return Rnd;
   end Random;

   ------------
   -- Random --
   ------------

   function Random return Unsigned_32
   is
      function Rnd is new Generic_Random (Unsigned_32);
   begin
      return Rnd;
   end Random;


   ------------
   -- Random --
   ------------

   function Random return Unsigned_64
   is
      function Rnd is new Generic_Random (Unsigned_64);
   begin
      return Rnd;
   end Random;

end Strong_Random;

 --  -- Random_Byte --
 --    -----------------
 --
 --    procedure Random_Byte (Result : out Interfaces.Unsigned_8)
 --    is
 --    begin
 --       if Bytes_Available = 0 then
 --          Buffer := Low_Level.Random_Word;
 --          Bytes_Available := Low_Level.Word'Size / 8;
 --       end if;
 --
 --       pragma Assert (Bytes_Available > 0);
 --
 --       Result := Interfaces.Unsigned_8 (Buffer mod 256);
 --       Buffer := Buffer / 256;
 --
 --       Bytes_Available := Bytes_Available - 1;
 --    end Random_Byte;
 --
 --    procedure Append (Buffer : in out Low_Level.Word;
 --                      Byte   : Interfaces.Unsigned_8)
 --    is
 --    begin
 --       Buffer := Buffer * 256 + Low_Level.Word (Byte);
 --    end Append;
 --
 --    ------------
 --    -- Random --
 --    ------------
 --
 --    function Random return Interfaces.Unsigned_8 is
 --       Result : Interfaces.Unsigned_8;
 --    begin
 --       Random_Byte (Result);
 --
 --       return Result;
 --    end Random;
 --
 --    ------------
 --    -- Random --
 --    ------------
 --
 --    function Random return Interfaces.Unsigned_16 is
 --       Result : Low_Level.Word := 0;
 --       Tmp    : Interfaces.Unsigned_8;
 --    begin
 --       for K in 1 .. 2 loop
 --          Random_Byte (Tmp);
 --          Append (Result, Tmp);
 --       end loop;
 --
 --       return Interfaces.Unsigned_16 (Result);
 --    end Random;
 --
 --    ------------
 --    -- Random --
 --    ------------
 --
 --    function Random return Interfaces.Unsigned_32 is
 --       Result : Low_Level.Word := 0;
 --       Tmp    : Interfaces.Unsigned_8;
 --    begin
 --       for K in 1 .. 4 loop
 --          Random_Byte (Tmp);
 --          Append (Result, Tmp);
 --       end loop;
 --
 --       return Interfaces.Unsigned_32 (Result);
 --    end Random;
 --
 --    ------------
 --    -- Random --
 --    ------------
 --
 --    function Random return Interfaces.Unsigned_64 is
 --       Result : Low_Level.Word := 0;
 --       Tmp    : Interfaces.Unsigned_8;
 --    begin
 --       for K in 1 .. 8 loop
 --          Random_Byte (Tmp);
 --          Append (Result, Tmp);
 --       end loop;
 --
 --       return Interfaces.Unsigned_64 (Result);
 --    end Random;

