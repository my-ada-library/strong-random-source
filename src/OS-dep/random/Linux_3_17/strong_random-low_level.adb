pragma Ada_2012;
with Ada.Unchecked_Conversion;
with Interfaces.C.Pointers;
with GNAT.OS_Lib;

package body Strong_Random.Low_Level is
   use Interfaces.C;

   ----------
   -- Fill --
   ----------

   function Random_Byte return Byte
   is
      type Byte_Array is array (Positive range <>) of aliased Byte;

      package Byte_Pointers is
        new Interfaces.C.Pointers (Index              => Positive,
                                   Element            => Byte,
                                   Element_Array      => Byte_Array,
                                   Default_Terminator => 0);

      type Ssize_T is range -Size_T'Modulus / 2 .. Size_T'Modulus / 2 - 1;

      function Get_Random (Buffer : Byte_Pointers.Pointer;
                           Length : Size_T;
                           Flags  : Unsigned)
                           return Ssize_T
        with Import, Convention => C,  External_Name => "getrandom";

      Err : Ssize_T;

      Buffer : Byte_Array (1 .. 1);
      Pt_Buffer : constant Byte_Pointers.Pointer :=
                    Byte_Pointers.Pointer'(Buffer (Buffer'First)'Access);
   begin
      Err := Get_Random (Buffer => Pt_Buffer,
                         Length => Buffer'Size / Byte'Size,
                         Flags => 0);

      if Err < 0 then
         raise Constraint_Error with GNAT.OS_Lib.Errno_Message;
      end if;

      if Err /= Buffer'Size / Byte'Size then
         --  This should never happen since getrandom guarantees to fill the
         --  buffer if its length is not larger than 256
         raise Program_Error;
      end if;

      return Buffer(Buffer'First);
   end Random_Byte;
end Strong_Random.Low_Level;
