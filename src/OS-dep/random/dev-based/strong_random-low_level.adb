with Ada.Sequential_IO;
with Ada.Text_IO; use Ada.Text_IO;
with Strong_Random.Low_Level.Random_Device;

package body Strong_Random.Low_Level is
   package Byte_Io is
     new Ada.Sequential_IO (Byte);

   Random_Source : Byte_Io.File_Type;

   function Random_Byte return Byte
   is
      Result : Byte;
   begin
      pragma Assert (Byte_Io.Is_Open (Random_Source));

      Byte_Io.Read (Random_Source, Result);

      return Result;
   end Random_Byte;

begin
   Byte_Io.Open (File => Random_Source,
                 Mode => Byte_Io.In_File,
                 Name => Random_Device.Name);
end Strong_Random.Low_Level;
