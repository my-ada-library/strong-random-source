with System;
with Interfaces;

--
--  This package is a low-level package that generates a random bit string
--  in a cryptographically strong way.  It can use OS resources; for example,
--  in recent Linux it calls C function getrandom().
--
--  Because of its dependency on the available OS resources, the body
--  the body will differ, depending on the OS  Note that the specs are
--  OS independent.
--
package Strong_Random is
   generic
      type Data_Type is mod <>;
   function Generic_Random return Data_Type;

   function Random return Interfaces.Unsigned_8;
   function Random return Interfaces.Unsigned_16;
   function Random return Interfaces.Unsigned_32;
   function Random return Interfaces.Unsigned_64;
end Strong_Random;
